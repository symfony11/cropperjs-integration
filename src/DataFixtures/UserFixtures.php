<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 51; $i++) {
            $user = new User;
            $user
                ->setUsername($faker->userName)
                ->setPassword($faker->word)
                ->setAvatar("default.png")
            ;
            $manager->persist($user);
        }

        $manager->flush();
    }
}
