<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class UserController extends AbstractController
{
    /**
     * @Route("/users/{userId}", name="user_details", requirements={"userId"="\d+"})
     * @Method("GET")
     * @ParamConverter("user", class="App\Entity\User", options={"id"="userId"})
     */
    public function show(Request $request, User $user)
    {
        dump($user);die;
    }
}
