<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PagesController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     * @Method({"GET","POST"})
     */
    public function home(Request $request, EntityManagerInterface $em): Response
    {
        $user = new User;
        $userForm = $this->createForm(UserFormType::class, $user);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $em->persist($user);
            $em->flush();
        }
        return $this->render('pages/home.html.twig', [
            'user_form' => $userForm->createView(),
        ]);
    }
}
